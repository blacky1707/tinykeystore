<?php

/**
 * LICENCE
 *
 * @copyright (c)20012-2013, Dirk Schwarz (http://www.dirk-schwarz.net)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

require_once 'Auth/User.php';
require_once 'Auth/Exception.php';

class KeyStore_Auth
{
    //genereal permissions
    const USER_PERMISSION_NO_PERMISSION         = 0x0;

    const USER_PERMISSION_READ                  = 0x1;

    const USER_PERMISSION_WRITE                 = 0x2;

    const USER_PERMISSION_DELETE                = 0x4;

    const USER_PERMISSION_CREATE                = 0x8;

    const USER_PERMISSION_DROP                  = 0x10;

    const USER_PERMISSION_GRANT                 = 0x20;

    const USER_PERMISSION_MANAGE_USER           = 0x40;

    //schema permission constants
    const USER_PERMISSION_SCHEMA_READ           = 0x1;

    const USER_PERMISSION_SCHEMA_WRITE          = 0x2;

    const USER_PERMISSION_SCHEMA_DELETE         = 0x4;

    //there can be no drop or create permission on schema level

    const USER_PERMISSION_SCHEMA_GRANT          = 0x8;

    const USER_PERMISSION_SCHEMA_MANAGE_USER    = 0x10;

    const USER_PERMISSION_SCHEMA_ROOT           = 0x800;

    //global permissions
    const USER_PERMISSION_GLOBAL_READ           = 0x1000;

    const USER_PERMISSION_GLOBAL_WRITE          = 0x2000;

    const USER_PERMISSION_GLOBAL_DELETE         = 0x4000;

    const USER_PERMISSION_GLOBAL_CREATE         = 0x8000;

    const USER_PERMISSION_GLOBAL_DROP           = 0x10000;

    const USER_PERMISSION_GLOBAL_GRANT          = 0x20000;

    const USER_PERMISSION_GLOBAL_MANAGE_USER    = 0x40000;

    const USER_PERMISSION_GLOBAL_ROOT           = 0x1000000;

    /**
     * instance of this class (singleton)
     * @var KeyStore_Auth
     */
    private static $_instance           = null;

    /**
     * instance of the systemStore
     *
     * @var KeyStore
     */
    private $_systemStore               = null;

    /**
     * current logged in user
     *
     * @var KeyStore_Auth_User
     */
    private $_currentUser               = null;

    /**
     * constructor (called by singleton)
     *
     * @param KeyStore $systemStore
     */
    function __construct(KeyStore $systemStore)
    {
        $this->_systemStore = $systemStore;
    }

    /**
     * get (or create) the instance of the Auth class
     *
     * @param KeyStore $systemStore
     * @return \KeyStore_Auth
     */
    public static function getInstance(KeyStore $systemStore)
    {
        if (empty(self::$_instance) == true) {
            self::$_instance = new KeyStore_Auth($systemStore);
        }

        return self::$_instance;
    }

    /**
     * returns an instance of the user
     * @param type $userName
     * @return \KeyStore_Auth_User
     */
    public function getUser($userName = null)
    {
        if (empty($userName) == true) {
            return new KeyStore_Auth_User();
        } else {
            $this->_systemStore->useSchema(KeyStore::USER_SCHEMA_NAME);
            $userData = $this->_systemStore->read($userName);

            return new KeyStore_Auth_User($userData);
        }
    }

    /**
     * saves the user to the system store
     *
     * @param KeyStore_Auth_User $user
     */
    public function updateUser(KeyStore_Auth_User $user)
    {
        if ($this->userExists($user->getUserName()) == true) {
            $this->_saveUser($user);
        } else {
            throw new KeyStore_Auth_Exception(
                'trying to update a user ('. $user->getUserName()
                . ') which does not exist'
            );
        }
    }

    /**
     * save user data
     *
     * @param KeyStore_Auth_User $user
     */
    private function _saveUser(KeyStore_Auth_User $user)
    {
        $this->_systemStore->useSchema(KeyStore::USER_SCHEMA_NAME);

        $this->_systemStore->write(
            $user->getUserName(),
            $user->toString()
        );
    }

    /**
     * add a user
     *
     * @param KeyStore_Auth_User $user
     * @throws KeyStore_Auth_Exception
     */
    public function addUser(KeyStore_Auth_User $user, $superId = null)
    {
        $this->_systemStore->useSchema(KeyStore::SYSTEM_SCHEMA_NAME);

        $superIdPass = (bool) (
            $this->_systemStore->read(
                '__superId__'
            ) == $superId
        );

        if ($superIdPass == true || $this->checkPermission(
            KeyStore_Auth::USER_PERMISSION_MANAGE_USER,
            $this->_currentUser,
            null
        )) {
            if ($this->userExists($user->getUserName()) == false) {
                $this->_saveUser($user);
            } else {
                throw new KeyStore_Auth_Exception('user already exists');
            }
        } else {
            throw new KeyStore_Auth_Exception(
                'no "manage user" permission for user "'
                . $this->_currentUser->getUserName() . '"'
            );
        }
    }

    /**
     * delete a user
     *
     * @param string $userName
     * @throws KeyStore_Auth_Exception
     */
    public function deleteUser($userName)
    {
        if ($this->checkPermission(
            KeyStore_Auth::USER_PERMISSION_MANAGE_USER,
            $this->_currentUser,
            null
        )) {

            if ($this->userExists($userName) == true) {
                $this->_systemStore->useSchema(KeyStore::USER_SCHEMA_NAME);

                $this->_systemStore->delete(
                    $userName
                );
            } else {
                throw new KeyStore_Auth_Exception('user does not exist');
            }
        } else {
            throw new KeyStore_Auth_Exception(
                'no manage user permission for user "'
                . $this->_currentUser->getUserName() . '"'
            );
        }
    }

    /**
     * check if a user exists
     *
     * @param string $userName
     * @return bool
     */
    public function userExists($userName)
    {
        $this->_systemStore->useSchema(KeyStore::USER_SCHEMA_NAME);
        $exists =  $this->_systemStore->exists($userName);

        return $exists;
    }

    /**
     * check permission
     *
     * @param integer $requiredPermission
     * @param KeyStore_Auth_User $user
     * @param string $schema
     * @return boolean
     */
    public function checkPermission(
        $requiredPermission,
        $user,
        $schema = null)
    {

        if ($user instanceof KeyStore_Auth_User) {
            $currentPermission = $user->getPermission();

            //if user is global root
            if ($this->_checkPermissionMask(
                self::USER_PERMISSION_GLOBAL_ROOT,
                $currentPermission['global']
            ) == true) {
                return true;
            }

            //set permission equivalences
            $permissionMap = array(
                self::USER_PERMISSION_READ => array(
                    'global' => self::USER_PERMISSION_GLOBAL_READ,
                    'schema' => self::USER_PERMISSION_SCHEMA_READ
                ),
                self::USER_PERMISSION_WRITE => array(
                    'global' => self::USER_PERMISSION_GLOBAL_WRITE,
                    'schema' => self::USER_PERMISSION_SCHEMA_WRITE
                ),
                self::USER_PERMISSION_DELETE => array(
                    'global' => self::USER_PERMISSION_GLOBAL_DELETE,
                    'schema' => self::USER_PERMISSION_SCHEMA_DELETE
                ),
                self::USER_PERMISSION_CREATE => array(
                    'global' => self::USER_PERMISSION_GLOBAL_CREATE,
                    'schema' => null
                ),
                self::USER_PERMISSION_DROP => array(
                    'global' => self::USER_PERMISSION_GLOBAL_DROP,
                    'schema' => null
                ),
                self::USER_PERMISSION_GRANT => array(
                    'global' => self::USER_PERMISSION_GLOBAL_GRANT,
                    'schema' => self::USER_PERMISSION_SCHEMA_GRANT
                ),
                self::USER_PERMISSION_MANAGE_USER => array(
                    'global' => self::USER_PERMISSION_GLOBAL_MANAGE_USER,
                    'schema' => self::USER_PERMISSION_SCHEMA_MANAGE_USER
                ),
            );

            if (empty($permissionMap[$requiredPermission]) == true) {
                throw new KeyStore_Auth_Exception(
                    'checking invalid permission'
                );
            }

            if ($this->_checkPermissionMask(
                $permissionMap[$requiredPermission]['global'],
                $currentPermission['global']
            )) {
                return true;
            } else {
                if (is_null($schema) == false) {
                    if (empty(
                        $currentPermission['schema'][$schema]
                    ) == false) {
                        if ($this->_checkPermissionMask(
                            KeyStore_Auth::USER_PERMISSION_SCHEMA_ROOT,
                            $currentPermission['schema'][$schema]
                        ) == true) {
                            return true;
                        }

                        if ($this->_checkPermissionMask(
                            $permissionMap[$requiredPermission]['schema'],
                            $currentPermission['schema'][$schema]
                        )) {

                            return true;
                        }
                    }
                }
            }

            return false;
        } else {
            throw new KeyStore_Auth_Exception('no user logged in');
        }
    }

    /**
     * authenticate using credentials
     *
     * @param string $userName
     * @param string $password
     * @return boolean
     */
    public function login($userName, $password)
    {
        $userObject = $this->getUser($userName);

        if ($userObject->checkPassword($password) == true) {
            $this->_currentUser = $userObject;

            return true;
        } else {
            return false;
        }
    }

    /**
     * logout from the system
     */
    public function logout()
    {
        $this->_currentUser = null;
    }

    /**
     * get the currentl loggen in user
     * @return KeyStore_Auth_User
     */
    public function getCurrentUser()
    {
        return $this->_currentUser;
    }

    /**
     * check the bitmask
     *
     * @param integer $permissionToCheck
     * @param integer $bitMask
     * @return bool
     */
    private function _checkPermissionMask($permissionToCheck, $bitMask)
    {
        $result = (bool) ($bitMask & $permissionToCheck);

        return $result;
    }
}