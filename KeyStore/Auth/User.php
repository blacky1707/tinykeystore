<?php

/**
 * LICENCE
 *
 * @copyright (c)20012-2013, Dirk Schwarz (http://www.dirk-schwarz.net)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

require_once 'User/Exception.php';

class KeyStore_Auth_User
{
    /**
     * contains the permissions of a user
     *
     * @var int Bitmask
     */
    private $_permission    = array(
        'global'    => 0,
        'schema'   => array('default' => 0),
    );

    /**
     * contains the unser name
     *
     * @var stirng
     */
    private $_userName      = null;

    /**
     * contains the password hash
     *
     * @var string
     */
    private $_password      = null;

    /**
     * contructor
     *
     * @param string $userData
     */
    function __construct($userData = null)
    {
        if (is_null($userData) == true) {
            $userData = serialize(array());
        }

        $this->_initUser($userData);
    }

    /**
     * returns the user as a serialized string
     *
     * @return string
     */
    public function toString()
    {
        $variables = get_class_vars(__CLASS__);

        $valueArray = array();
        foreach ($variables as $key => $value) {
            $valueArray[$key] = $this->$key;
        }

        return serialize($valueArray);
    }

    /**
     * initializes the class (properties are set)
     *
     * @param string $userData
     */
    private function _initUser($userData)
    {
        $userDataArray = unserialize($userData);

        foreach ($userDataArray as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * set a new password
     *
     * @param string $plainPassword
     */
    public function setPassword($plainPassword)
    {
        $this->_password = $this->_createPassword($plainPassword);
    }

    /**
     * set the permission
     *
     * has to be an array
     *
     * $permission['global'] contains global permissions
     * $permission['schema']['myschema'] contains the schema permission
     * @todo permission to __schema__ according to current permission
     *
     * @param array $permission
     */
    public function setPermission(Array $permission)
    {
        if (empty($permission['global']) == false) {
            $this->_permission['global'] = $permission['global'];
        }

        if (empty($permission['schema']) == false) {
            foreach ($permission['schema'] as $itemKey => $itemValue) {
                $this->_permission['schema'][strtolower($itemKey)] = $itemValue;
            }
        }

        $this->_setCollectionPermission();
    }

    /**
     * set the permission for the collection schema
     */
    private function _setCollectionPermission()
    {
        $collectionPermission = KeyStore_Auth::USER_PERMISSION_SCHEMA_READ
            | KeyStore_Auth::USER_PERMISSION_SCHEMA_WRITE
            | KeyStore_Auth::USER_PERMISSION_SCHEMA_DELETE;

        $this->_permission['schema']['__collection__'] = $collectionPermission;
    }

    /**
     * magic function for creating getters and setters
     *
     * @param string $name
     * @param array $arguments
     * @return mixed
     * @throws KeyStore_Auth_User_Exception
     */
    public function __call($name, $arguments)
    {
        $propertyName = '_' . lcfirst(substr($name, 3));

        if (property_exists($this, $propertyName) == true) {
            if (substr($name, 0, 3) == 'set') {
                $this->$propertyName = $arguments[0];
                return;
            }

            if (substr($name, 0, 3) == 'get') {
                return $this->$propertyName;
            }
        }

        throw new KeyStore_Auth_User_Exception(
            'undefined property "' . (str_replace('_', '', $propertyName)) . '"'
        );
    }

    /**
     * create a salted password hash
     *
     * @param string $plainPassword
     * @return string
     */
    private function _createPassword($plainPassword, $salt = null)
    {
        if (empty($salt) == true) {
            $salt = substr(sha1(microtime()), 0, 4);
        }

        $crypted = sha1($salt . $plainPassword);
        $salted = $crypted . ':' . $salt;

        return $salted;
    }

    /**
     * check if password does match
     *
     * @param string $plainPassword
     * @param string $hashedPassword
     * @return bool
     */
    public function checkPassword($plainPassword)
    {
        $hashParts = explode(':', $this->_password);
        $salted = $this->_createPassword($plainPassword, $hashParts[1]);

        return (bool)($this->_password == $salted);
    }
}