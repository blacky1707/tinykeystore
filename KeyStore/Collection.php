<?php

/**
 * LICENCE
 *
 * @copyright (c)20012-2013, Dirk Schwarz (http://www.dirk-schwarz.net)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

class KeyStore_Collection
{
    const COLLECTION_TYPE_SIMPLE            = 1;

    const COLLECTION_TYPE_SORTABLE          = 2;

    const COLLECTION_TYPE_HIERARCHICAL      = 3;

    /**
     * contains data of the collection
     *
     * @var array
     */
    private $_data                          = null;

    /**
     * array with header information
     *
     * @var array
     */
    private $_header                        = null;

    /**
     * current pointer position
     *
     * @var int
     */
    private $_pointerPosition               = null;

    /**
     * constructor
     *
     * @param string $record serialized record
     * @param string $collectionName name of the collections
     */
    function __construct(
        $record         = null,
        $collectionName = null,
        $collectionType = KeyStore_Collection::COLLECTION_TYPE_SIMPLE)
    {
        $this->_data            = array();
        $this->_header          = array();

        if (is_null($record) == false) {
            $recordArray = unserialize($record);

            $this->_data    = $recordArray['data'];
            $this->_header  = $recordArray['header'];

            if (empty($this->_data[0]) == false) {
                //collection has records
                $this->_pointerPosition = 0;
            } else {
                //collection is empty
                $this->_pointerPosition = -1;
            }
        } else {
            if (is_null($collectionName) == false) {
                $this->_header['name'] = $collectionName;
            } else {
                throw new KeyStore_Collection_Exception(
                    'a new collection must have a name'
                );
            }

            $this->_header['type'] = $collectionType;

            $this->_pointerPosition = -1;
        }
    }

    /**
     * create serialized string of collection
     *
     * @return type
     */
    public function toString()
    {
        $record = array(
            'header'    => $this->_header,
            'data'      => $this->_data
        );

        return serialize($record);
    }

    /**
     * go to next record
     *
     * @return \KeyStore_Collection
     * @throws KeyStore_Collection_Exception
     */
    public function next()
    {
        if ($this->isEnd() == false) {
            if ($this->_counterPosition + 1 < count($this->_data)) {
                $this->_counterPosition ++;
            } else {
                throw new KeyStore_Collection_Exception(
                    'End of collection reached'
                );
            }
        }

        return $this;
    }

    /**
     * go to previous record
     *
     * @return \KeyStore_Collection
     * @throws KeyStore_Collection_Exception
     */
    public function previous()
    {
        if ($this->_pointerPosition > 0) {
            $this->_pointerPosition --;
        } else {
            throw new KeyStore_Collection_Exception(
                'First record of collection reached'
            );
        }

        return $this;
    }

    /**
     * go to end of collection
     *
     * @return \KeyStore_Collection
     */
    public function end()
    {
        $this->_pointerPosition = (count($this->_data) - 1);

        return $this;
    }

    /**
     * go to beginning of collection
     *
     * @return \KeyStore_Collection
     */
    public function start()
    {
        if ((count($this->_data) - 1) != -1) {
            $this->_pointerPosition = 0;
        } else {
            $this->_pointerPosition = -1;
        }

        return $this;
    }

    /**
     * check if end of collection is reached
     *
     * @return bool
     */
    public function isEnd()
    {
        return (bool) ($this->_pointerPosition == (count($this->_data) - 1));
    }

    /**
     * get count of collection items
     *
     * @return int
     */
    public function getCount()
    {
        return count($this->_data);
    }

    /**
     * add item to collection
     *
     * @param string $key
     */
    public function add($key)
    {
        $this->_data[] = $key;
    }

    /**
     * get record of collection
     * @return string
     * @throws KeyStore_Collection_Exception
     */
    public function getItem($position = 0)
    {
        if ($this->_pointerPosition <> -1) {
            return $this->_data[$this->_pointerPosition];
        } else {
            throw new KeyStore_Collection_Exception('recordset is empty');
        }
    }

    /**
     * get the name of the collection
     *
     * @return string
     */
    public function getName()
    {
        return $this->_header['name'];
    }

    /**
     * get list of all items
     *
     * @return array
     */
    public function getList()
    {
        return $this->_data;
    }

    /**
     * get the pointer position
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->_pointerPosition;
    }
}