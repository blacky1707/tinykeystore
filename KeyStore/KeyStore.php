<?php

/**
 * LICENCE
 *
 * @copyright (c)20012-2013, Dirk Schwarz (http://www.dirk-schwarz.net)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

require_once 'Exception.php';
require_once 'Version.php';
require_once 'Auth.php';
require_once 'Auth/Exception.php';
require_once 'Collection.php';

class KeyStore
{
    const TEST_VALUE                = '42 :-)';

    const USER_SCHEMA_NAME          = 'user';

    const SYSTEM_SCHEMA_NAME        = 'system';

    /**
     * path to the store
     *
     * @var string
     */
    private $_storePath             = null;

    /**
     * hash algorithm used
     *
     * @see http://php.net/manual/en/function.hash.php
     * @var string
     */
    private $_hashAlgorithm         = null;

    /**
     * length of the used hash
     *
     * @var int
     */
    private $_hashLength            = null;

    /**
     * calculated step size for folder creation
     *
     * @var int
     */
    private $_stepSize              = null;

    /**
     * folder depth of storage
     *
     * @var integer
     */
    private $_folderDepth           = null;

    /**
     * is history enabled
     *
     * @var bool
     */
    private $_historyEnabled        = null;

    /**
     * should the history be preserved after deletion
     *
     * @var bool
     */
    private $_keepHistory           = null;

    /**
     * should encryption be used
     *
     * @var bool
     */
    private $_useEncryption         = null;

    /**
     * the key to encrypt/decrypt
     *
     * @var string
     */
    private $_encryptionKey         = null;

    /**
     * encryption method to use (mcrypt)
     *
     * @var string
     */
    private $_encryptionMethod      = null;

    /**
     * encryption mode to use (mcrypt)
     *
     * @var string
     */
    private $_encryptionMode        = null;

    /**
     * if the KeyStore is the internal one
     *
     * @var bool
     */
    private $_isInternal            = null;

    /**
     * initialization vector for encryption
     * @var type
     */
    private $_storeEncryptionIv     = null;

    /**
     * instance of KeyStore for internal use (mcrypt)
     *
     * @var KeyStore
     */
    private $_systemStore           = null;

    /**
     * if user management is enabled
     *
     * @var bool
     */
    private $_enableUserManagement  = null;

    /**
     * name of the default schema
     * @var string
     */
    private $_defaultSchema         = null;

    /**
     * current active schema
     *
     * @var string
     */
    private $_currentSchema         = null;

    /**
     * instance of the Auth class
     *
     * @var KeyStore_Auth
     */
    private $_auth                  = null;

    /**
     * cache for paths
     * @var array
     */
    private $_keyCache              = array();

    /**
     *  list of possible config params and default values
     *
     * @var array
     */
    private $_paramValues = array(
        'storePath'                 => null,
        'hashAlgorithm'             => 'sha1',
        'folderDepth'               => 4,
        'historyEnabled'            => false,
        'keepHistory'               => false,
        'useEncryption'             => false,
        'encryptionKey'             => 'ChangeMe!',
        'encryptionMethod'          => MCRYPT_TWOFISH,
        'encryptionMode'            => MCRYPT_MODE_CBC,
        'enableUserManagement'      => true,
        'isInternal'                => false,
        'defaultSchema'             => 'default'
    );

    /**
     * contains the superID, which can be used to create a user (like root)
     * @var string
     */
    private $_superId               = null;

    /**
     * constructor: setup the key store
     *
     * config may contain the following settings:
     *
     * string storePath
     * [optional] string  hashAlgorithm         = 'sha1'
     * [optional] int     folderDepth           = 4
     * [optional] bool    historyEnabled        = false
     * [optional] bool    keepHistory           = false
     * [optional] bool    useEncryption         = false
     * [optional] string  encryptionKey         = 'MySecretKey'
     * [optional] string  encryptionMethod      = MCRYPT_TWOFISH
     * [optional] string  encryptionMode        = MCRYPT_MODE_CBC
     * [optional] bool    enableUserManagement  = false
     * [interna]  bool    isInternal            = false
     *
     * @param array $config
     */
    function __construct($config)
    {
        $this->_initSettings($config);

        if ($this->_isInternal == false) {
            $this->_startSystemStore();
        }

        if ($this->_useEncryption == true) {
            $this->_setupEncryption();
        }

        if ($this->_isInternal == false) {
            $this->_testStore();
        }

        if ($this->_enableUserManagement == true) {
            $this->_checkDefaultUser();
        }

        if ($this->_isInternal == false) {
            $this->_checkDefaultSchema();
            $this->_checkCollectionSchema();
        } else {
            if ($this->schemaExists(KeyStore::SYSTEM_SCHEMA_NAME) == false) {
                $this->createSchema(KeyStore::SYSTEM_SCHEMA_NAME);
            }

            if ($this->schemaExists(KeyStore::USER_SCHEMA_NAME) == false) {
                $this->createSchema(KeyStore::USER_SCHEMA_NAME);
            }
        }
    }

    /**
     * check and create the collection schema
     */
    private function _checkCollectionSchema()
    {
        if ($this->schemaExists('__collection__') == false) {
            if ($this->_enableUserManagement == true) {
                $this->connect('root', 'root');
            }

            //create the collection schema
            $this->createSchema('__collection__');

            //diconnect root
            if ($this->_enableUserManagement == true) {
                $this->disconnect();
            }
        }
    }

    /**
     * checks and creates the default schema
     */
    private function _checkDefaultSchema()
    {
        if ($this->schemaExists($this->_defaultSchema) == false) {
            //login with default too to create schema
            if ($this->_enableUserManagement == true) {
                $this->connect('root', 'root');
            }

            //create the default schema
            $this->createSchema($this->_defaultSchema);

            //diconnect root
            if ($this->_enableUserManagement == true) {
                $this->disconnect();
            }
        }
    }

    /**
     * checks for root user and creates it if it does not exist
     */
    private function _checkDefaultUser()
    {
        $this->_auth = KeyStore_Auth::getInstance($this->_systemStore);

        if ($this->_auth->userExists('root') == false) {
            $userRecord = $this->_auth->getUser();
            $userRecord->setUserName('root');
            $userRecord->setPassword('root');
            $userRecord->setPermission(
                array(
                    'global' => KeyStore_Auth::USER_PERMISSION_GLOBAL_ROOT
                )
            );

            //create default user with superId
            $this->_auth->addUser($userRecord, $this->_superId);
        }
    }

    /**
     * connect with credentials to the store
     *
     * @param string $userName
     * @param string $password
     * @return boolean
     */
    public function connect($userName, $password)
    {
        return $userObject = $this->_auth->login($userName, $password);
    }

    /**
     * diconnect the current user
     */
    public function disconnect()
    {
        $this->_auth->logout();
    }

    /**
     * read data by key
     *
     * @param string $key
     * @param int $revision
     *
     * @return string (or null if failed)
     */
    public function read($key, $revision = null)
    {
        if ($this->_enableUserManagement == false
            || $this->_isInternal
            || $this->_auth->checkPermission(
                KeyStore_Auth::USER_PERMISSION_READ,
                $this->_auth->getCurrentUser(),
                $this->_currentSchema
            ) == true) {

            $filePath = $this->_getFilePath($key);

            if (file_exists($filePath) == true || $revision != null) {
                if (empty($revision) == true) {
                    $data = file_get_contents($filePath);
                } else {
                    $revisionFileName = $filePath . '.r' . $revision;

                    if (file_exists($revisionFileName) == true) {
                        $data = file_get_contents($revisionFileName);
                    } else {
                        if (file_exists($filePath) == true && $revision == 1) {
                            $data = file_get_contents($filePath);
                        } else {
                            throw new KeyStore_Exception(
                                'key "' . $key . '" does not exist'
                            );
                        }
                    }
                }

                if ($this->_useEncryption == true) {
                    $decryptedData = $this->_decryptData($data);
                    $value = trim($decryptedData);
                } else {
                    $value = $data;
                }
            } else {
                throw new KeyStore_Exception(
                    'key "' . $key . '" does not exist'
                );
            }

            return $value;
        } else {
            throw new KeyStore_Auth_Exception(
                'no read permission for user "'
                . $this->_auth->getCurrentUser()->getUserName() . '"'
            );
        }
    }

    /**
     * check if a key exists
     *
     * @todo check regarding required read permission
     *
     * @param string $key
     * @return bool
     */
    public function exists($key)
    {
        $filePath = $this->_getFilePath($key);

        return (bool) (file_exists($filePath) === true);
    }

    /**
     * write data to the store
     *
     * @param string $key
     * @param mixed $data
     */
    public function write($key, $data)
    {
        if ($this->_enableUserManagement == false
            || $this->_isInternal
            || $this->_auth->checkPermission(
                KeyStore_Auth::USER_PERMISSION_WRITE,
                $this->_auth->getCurrentUser(),
                $this->_currentSchema
            ) == true) {

            $filePath = $this->_getFilePath($key);
            $basePath = dirname($filePath);

            if (is_dir($basePath) == false) {
                mkdir($basePath, 0777, true);
            }

            //this is the faster way of updating data (delete and re-create)
            //updating an existing file seems to be slower
            if (file_exists($filePath) == true) {
                if ($this->_historyEnabled == true) {
                    $this->_updateRevision($key);
                } else {
                    unlink($filePath);
                }
            }

            if ($this->_useEncryption == true) {
                $dataToSave = $this->_encryptData($data);
            } else {
                $dataToSave = $data;
            }

            $fpData = fopen($filePath, 'w');
            fwrite($fpData, $dataToSave);
            fclose($fpData);
        } else {
            throw new KeyStore_Auth_Exception(
                'no write permission for user "'
                . $this->_auth->getCurrentUser()->getUserName() . '"'
            );
        }
    }

    /**
     * get collection
     *
     * @param string $collectionName
     * @return \KeyStore_Collection
     * @throws KeyStore_Exception
     */
    public function getCollection($collectionName)
    {
        $oldSchema = $this->_currentSchema;

        $this->useSchema('__collection__');

        if ($this->exists($collectionName) == false) {
            return new KeyStore_Collection(null, $collectionName);
        } else {
            $collectionData = $this->read($collectionName);

            return new KeyStore_Collection($collectionData, $collectionName);
        }

        $this->useSchema($oldSchema);
    }

    /**
     * saves a collection
     *
     * @param KeyStore_Collection $collection
     */
    public function saveCollection(KeyStore_Collection $collection)
    {
        $oldSchema = $this->_currentSchema;

        $this->useSchema('__collection__');

        $collectionData = $collection->toString();
        $this->write($collection->getName(), $collectionData);

        $this->useSchema($oldSchema);
    }

    /**
     * delete a key (and all revisions)
     *
     * @param string $key
     */
    public function delete($key)
    {
        if ($this->_enableUserManagement == false
            || $this->_isInternal
            || $this->_auth->checkPermission(
                KeyStore_Auth::USER_PERMISSION_DELETE,
                $this->_auth->getCurrentUser(),
                $this->_currentSchema
            ) == true) {

            $filePath = $this->_getFilePath($key);

            if (file_exists($filePath) == true) {
                if ($this->_historyEnabled == true) {
                    if ($this->_keepHistory == false) {
                        array_map('unlink', glob($filePath . '.*'));
                    } else {
                        $this->_updateRevision($key);
                        $headRevision = $this->getHeadRevisionNumber(
                            $key,
                            true
                        );
                        $this->_setHeadRevision(
                            $key,
                            $headRevision + 1
                        );
                    }
                } else {
                    unlink($filePath);
                }
            }
        } else {
            throw new KeyStore_Auth_Exception(
                'no delete permission for user "'
                . $this->_auth->getCurrentUser()->getUserName() . '"'
            );
        }
    }

    /**
     * returns the head revision number
     *
     * @todo check regarding required read permission
     *
     * @param string $key
     * @return int (or false if file does not exist or is deleted)
     */
    public function getHeadRevisionNumber(
        $key,
        $forceRead = false)
    {
        $revisionFilePath = $this->_getRevisionFilePath($key);
        $filePath = $this->_getFilePath($key);

        if (file_exists($revisionFilePath) == true) {
            if (file_exists($filePath) == true || $forceRead) {
                return file_get_contents($revisionFilePath);
            } else {
                return false;
            }
        } else {
            return 1;
        }
    }

    /**
     * gets the location of an item
     *
     * @check regarding required read permission
     *
     * @param string $key
     * @return string
     */
    public function getItemStorePath($key)
    {
        return $this->_getFilePath($key, $this->_currentSchema);
    }

    /**
     * get Auth instance
     *
     * @return KeyStore_Auth
     */
    public function getAuth()
    {
        if ($this->_enableUserManagement == true) {
            return $this->_auth;
        } else {
            throw new KeyStore_Exception(
                'trying to use atuh module even user management is disabled'
            );
        }
    }

    /**
     * returns the string representation of the version
     *
     * @return string
     */
    public function getVersion()
    {
        return KeyStore_Version::VERSION;
    }

    /**
     * check if a schema exists
     *
     * @todo check regarding required read permission
     *
     * @param string $schemaName
     * @return bool
     */
    public function schemaExists($schemaName)
    {
        $schemaPath = $this->_getSchemaPath($schemaName);

        return is_dir($schemaPath);
    }

    /**
     * create a schema
     *
     * @param string $schemaName
     * @throws KeyStore_Exception
     */
    public function createSchema($schemaName)
    {
        if ($this->_enableUserManagement == false
            || $this->_isInternal
            || $this->_auth->checkPermission(
                KeyStore_Auth::USER_PERMISSION_CREATE,
                $this->_auth->getCurrentUser(),
                $schemaName
            ) == true) {

            $schemaPath = $this->_getSchemaPath($schemaName);

            if ($this->schemaExists($schemaName) == false) {
                mkdir($schemaPath, 0777, true);
            } else {
                throw new KeyStore_Exception('schema already exists');
            }
        } else {
            throw new KeyStore_Exception(
                'no schema-create permission for user "'
                . $this->_auth->getCurrentUser()->getUserName() . '"'
            );
        }
    }

    /**
     * drop a schema
     *
     * @todo check for current schema
     *
     * @param string $schemaName
     * @throws KeyStore_Exception
     */
    public function dropSchema($schemaName)
    {
        if ($this->_enableUserManagement == false
            || $this->_isInternal
            || $this->_auth->checkPermission(
                KeyStore_Auth::USER_PERMISSION_DROP,
                $this->_auth->getCurrentUser(),
                $schemaName
            ) == true) {

            if ($this->schemaExists($schemaName) == true) {
                $schemaPath = $this->_getSchemaPath($schemaName);
                $this->_recursiveDeleteDirectory($schemaPath);
            } else {
                throw new KeyStore_Exception('schema does not exist');
            }
        } else {
            throw new KeyStore_Exception(
                'no schema-drop permission for user "'
                . $this->_auth->getCurrentUser()->getUserName() . '"'
            );
        }
    }

    /**
     * select schema to use
     *
     * @param string $schemaName
     * @throws KeyStore_Exception
     */
    public function useSchema($schemaName)
    {
        if ($this->schemaExists($schemaName) == true) {
            $this->_currentSchema = $schemaName;
        } else {
            throw new KeyStore_Exception('schema does not exist');
        }
    }

    /**
     * get the path to the schema
     *
     * @param string $schemaName
     * @return string
     */
    private function _getSchemaPath($schemaName)
    {
        $schemaPath = $this->_storePath . '/_schema/'
            . strtolower($schemaName);

        return $schemaPath;
    }

    /**
     * delete a schema recursively
     *
     * @param string $directoryPath
     */
    private function _recursiveDeleteDirectory($directoryPath)
    {
        foreach (glob($directoryPath . '/*') as $file) {
            if (is_dir($file)) {
                $this->_recursiveDeleteDirectory($file);
            } else {
                unlink($file);
            }
        }

        rmdir($directoryPath);
    }

    /**
     * read the settings from config array
     *
     * @param array $config
     * @throws KeyStore_Exception
     */
    private function _initSettings($config)
    {
        foreach ($this->_paramValues as $paramKey => $paramDefaultValue) {
            if ($paramDefaultValue === null) {
                if (isset($config[$paramKey]) == false) {
                    throw new KeyStore_Exception(
                        'required ' . $paramKey . ' not defined'
                    );
                }
            }

            $settingVariable = '_' . $paramKey;

            if (isset($config[$paramKey]) == true) {
                $this->$settingVariable = $config[$paramKey];
            } else {
                $this->$settingVariable = $paramDefaultValue;
            }
        }

        $this->_currentSchema = $this->_defaultSchema;
    }

    /**
     * start the internal system store
     */
    private function _startSystemStore()
    {
        $systemStoreConfig = array(
            'storePath'             => $this->_storePath . '/_system',
            'hashAlgorithm'         => $this->_hashAlgorithm,
            'folderDepth'           => $this->_folderDepth,
            //never use history for system store
            'historyEnabled'        => false,
            'keepHistory'           => false,
            'useEncryption'         => $this->_useEncryption,
            'encryptionKey'         => $this->_encryptionKey,
            'encryptionMethod'      => $this->_encryptionMethod,
            'encryptionMode'        => $this->_encryptionMode,
            'isInternal'            => true,     //this is the internal db
            'enableUserManagement'  => false,    //no user management
            'defaultSchema'         => 'system'
        );

        // create internal store
        $this->_systemStore = new KeyStore($systemStoreConfig);

        if ($this->_systemStore->exists('__superId__') == false) {
            $this->_superId = $this->_createSuperId();
            $this->_systemStore->write(
                '__superId__',
                $this->_superId
            );
        } else {
            $this->_superId = $this->_systemStore->read(
                '__superId__'
            );
        }
    }

    /**
     * create the superId
     *
     * @return string
     */
    private function _createSuperId()
    {
        $superId = hash(
            'sha512',
            microtime() . rand(1, 1000000000)
        ) . hash(
            'sha512',
            microtime() . rand(1, 1000000000)
        );

        return $superId;
    }

    private function _testStore()
    {
        if ($this->_systemStore->exists('__testKey__') == false) {
            $this->_systemStore->write(
                '__testKey__',
                self::TEST_VALUE
            );
        }

        $testValue = $this->_systemStore->read('__testKey__');

        if ($testValue != self::TEST_VALUE) {
            throw new KeyStore_Exception(
                'Store is not working. Can\'t read from systemStore.'
            );
        }
    }

    /**
     * setup encryption for store and system store
     */
    private function _setupEncryption()
    {
        if ($this->_isInternal == false) {
            $storePath = $this->_storePath . '/_system';
        } else {
            $storePath = $this->_storePath;
        }

        // check for directory and create it, if it does not exist
        if (is_dir($storePath) == false) {
            mkdir($storePath, 0777, true);
        }


        $ivFile = $storePath . '/.storeEncryptionIv';

        if (file_exists($ivFile) == false) {
            $this->_storeEncryptionIv = mcrypt_create_iv(
                mcrypt_get_iv_size(
                    $this->_encryptionMethod,
                    $this->_encryptionMode
                ),
                MCRYPT_DEV_RANDOM
            );

            $fpIvFile = fopen($ivFile, 'wb');
            fwrite($fpIvFile, $this->_storeEncryptionIv);
            fclose($fpIvFile);
        } else {
            $this->_storeEncryptionIv = file_get_contents($ivFile);
        }
    }

    /**
     * internal encryption function
     *
     * @param string $data
     * @return string
     */
    private function _encryptData($data)
    {
        $encryptedData = mcrypt_encrypt(
            $this->_encryptionMethod,
            md5($this->_encryptionKey),
            $data,
            $this->_encryptionMode,
            $this->_storeEncryptionIv
        );

        return $encryptedData;
    }

    /**
     * internal descrption function
     *
     * @param string $encryptedData
     * @return string
     */
    private function _decryptData($encryptedData)
    {
        $decrypted = mcrypt_decrypt(
            $this->_encryptionMethod,
            md5($this->_encryptionKey),
            $encryptedData,
            $this->_encryptionMode,
            $this->_storeEncryptionIv
        );

        return $decrypted;
    }

    /**
     * updates the revision file and moves former data to revision
     *
     * @param string $key
     */
    private function _updateRevision($key)
    {
        $filePath = $this->_getFilePath($key);

        $currentRevision = $this->getHeadRevisionNumber($key);
        rename($filePath, $filePath . '.r' . $currentRevision);
        $this->_setHeadRevision($key, $currentRevision + 1);
    }

    /**
     * set the head revision
     *
     * @param string $key
     * @param int $revision
     */
    private function _setHeadRevision($key, $revision)
    {
        $revisionFilePath = $this->_getRevisionFilePath($key);

        $fpRevision = fopen($revisionFilePath, 'w');
        fwrite($fpRevision, $revision);
        fclose($fpRevision);
    }

    /**
     * get the file path for a key
     *
     * @param string $key
     * @return string
     */
    private function _getFilePath($key)
    {
        if (empty($this->_keyCache[$key]) == false) {
            return $this->_keyCache[$key];
        } else {
            $keyHash = hash($this->_hashAlgorithm, $key);

            $path = $this->_storePath . '/_schema/'
                . strtolower(trim($this->_currentSchema));

            for (
                $i = 0;
                $i < $this->_getHashLength();
                $i += $this->_getStepSize()) {

                $path .= DIRECTORY_SEPARATOR . substr($keyHash, $i, 1);
            };

            $path .= DIRECTORY_SEPARATOR . $keyHash . '.dat';

            $this->_keyCache[$key] = $path;

            return $path;
        }
    }

    /**
     * get path for revision file
     *
     * @param string $key
     * @return string
     */
    private function _getRevisionFilePath($key)
    {
        $filePath = $this->_getFilePath($key);
        $revisionFilePath = $filePath . '.revision';

        return $revisionFilePath;
    }

    /**
     * get length of the hash string depending on the used algorithm
     *
     * @return int
     */
    private function _getHashLength()
    {
        if (empty($this->_hashLength) == true) {
            $hash = hash($this->_hashAlgorithm, 'test');
            $this->_hashLength = strlen($hash);
        }

        return $this->_hashLength;
    }

    /**
     * calculate the step size depending on the used hash algorithm
     *
     * @return int
     */
    private function _getStepSize()
    {
        if (empty($this->_stepSize) == true) {
            $stepSize = floor($this->_getHashLength() / $this->_folderDepth);
            $this->_stepSize = $stepSize;
        }

        return $this->_stepSize;
    }
}