<?php

/**
 * LICENCE
 *
 * @copyright (c)20012-2013, Dirk Schwarz (http://www.dirk-schwarz.net)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * tiny little stop-watch for testing purposes
 */
class StopWatch
{
    /**
     * starting micro-timestamp
     *
     * @var double
     */
    private $_start = null;

    /**
     * timestamp at the end
     *
     * @var double
     */
    private $_stop = null;

    /**
     * start measuring
     */
    public function start()
    {
        $this->_start = microtime(true);
    }

    /**
     * stop measuring
     *
     * @return double duration in seconds
     */
    public function stop()
    {
        $this->_stop = microtime(true);

        return $this->getDuration();
    }

    /**
     * calculates the duration in seconds
     *
     * @return double
     */
    public function getDuration()
    {
        return ($this->_stop - $this->_start);
    }
}