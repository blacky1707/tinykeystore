TinyKeyStore
============

What is TinyKeyStore?
---------------------
TinyKeyStore is a small key/value store based on PHP. It is easy to set-up, very fast, and easy to use.

IMPORTANT NOTE!!!
-----------------
This is a very early version of the software. function names, method params etc. may change in future releases!

Features
--------
* fast saving and retrieving of data
* history of data (revisions)
* encryption implemented (mcrypt)
* user management
* different schemas possible

Requirements
------------
TinyKeyStore requires the following:
* PHP 5.3.0 or higher
* A webserver which can run PHP
* mcrypt

Installation
------------
just copy the source and start ;-)

Configuration file(s)
---------------------
There are no config files at the moment

Known issues
------------
* currently the software is tested on Linux only
* not much samples or documentation available

Roadmap
-------
* 1-click setup
* SOAP/REST interface
* handle versions of key store (if format changes perform an update)
* Autoloading
* Dependency injection

Contact
-------
* Bitbucket:  [https://bitbucket.org/blacky1707/tinykeystore](https://bitbucket.org/blacky1707/tinykeystore)
* E-mail:  [wsmm.dirk@gmail.com](mailto:wsmm.dirk@gmail.com)

LICENCE
-------

Copyright (c)20012-2013, Dirk Schwarz (http://www.dirk-schwarz.net)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.