<?php

/**
 * LICENCE
 *
 * @copyright (c)20012-2013, Dirk Schwarz (http://www.dirk-schwarz.net)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * This file is for testing purposes only. It will be removed in future releases
 */


try {
    include 'KeyStore/KeyStore.php';

    $config = array(
        'storePath'         => '/var/www/store',
        'folderDepth'       => 2,
        'historyEnabled'    => true,
        'keepHistory'       => true,
        'useEncryption'     => false,
        'encryptionKey'     => '$3ThisIsaTestKeyForEncryption;#!-42'
    );

    //open key-store
    $keyValue = new KeyStore($config);

    if ($keyValue->getAuth()->userExists('test-user') == false) {
        $keyValue->connect('root', 'root');

        //create an empty user object
        $testUser = $keyValue->getAuth()->getUser();
        $testUser->setUserName('test-user');
        $testUser->setPassword('test');

        $permission = array(
            'global'            => KeyStore_Auth::USER_PERMISSION_GLOBAL_CREATE,
            'schema'            => array(
                'test-schema'   => KeyStore_Auth::USER_PERMISSION_SCHEMA_ROOT
            )
        );
        $testUser->setPermission($permission);
        $keyValue->getAuth()->addUser($testUser);

        $keyValue->disconnect();
    }

    $keyValue->connect('test-user', 'test');
    if ($keyValue->schemaExists('test-schema') == false) {
        $keyValue->createSchema('test-schema');
    }
    $keyValue->useSchema('test-schema');
    $keyValue->write('test', 'testData');

    echo $keyValue->read('test');
} catch (Exception $e) {
    echo $e->getMessage();
}