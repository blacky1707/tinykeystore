<?php

/**
 * LICENCE
 *
 * @copyright (c)20012-2013, Dirk Schwarz (http://www.dirk-schwarz.net)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

class KeyStoreTest extends PHPUnit_Framework_TestCase
{
    private $_config = array(
        'storePath'         => '/tmp/store_test',
        'folderDepth'       => 2,
        'historyEnabled'    => true,
        'keepHistory'       => true,
        'useEncryption'     => true,
        'encryptionKey'     => '$3ThisIsaTestKeyForEncryption;#!-42'
    );

    public function setUp()
    {
        mkdir('/tmp/store_test');

        parent::setUp();
    }

    public function tearDown()
    {
        shell_exec('rm -R /tmp/store_test');

        parent::tearDown();
    }

    public function testInitKeyStore()
    {
        $keyStore = $this->_setupTestKeyStore();

        $this->assertInstanceOf('KeyStore', $keyStore);
    }

    public function testVersion()
    {
        $keyStore = $this->_setupTestKeyStore();

        $currentVersion = $keyStore->getVersion();
        $result = KeyStore_Version::compareVersion($currentVersion);

        $this->assertEquals('0', $result);
    }

    public function testReadWrite()
    {
        $keyStore = $this->_setupTestKeyStore();
        $keyStore->write('test', 'hello world :-)');
        $result = $keyStore->read('test');

        $this->assertEquals('hello world :-)', $result);
    }

    public function testWriteWithoutHistory()
    {
        $config = $this->_config;
        $config['historyEnabled'] = false;

        $keyStore = $this->_setupTestKeyStore($config);
        $keyStore->write('test', 'hello world :-)');
        $result = $keyStore->read('test');

        $this->assertEquals('hello world :-)', $result);
    }


    public function testReadKeyDoesNotExist()
    {
        $this->setExpectedException('KeyStore_Exception');

        $keyStore = $this->_setupTestKeyStore();
        $result = $keyStore->read('where am i');
    }

    public function testReadWriteUncrypted()
    {
        $config = $this->_config;
        $config['useEncryption'] = false;

        $keyStore = $this->_setupTestKeyStore($config);
        $keyStore->write('test', 'hello world :-)');
        $result = $keyStore->read('test');

        $this->assertEquals('hello world :-)', $result);
    }


    public function testRevision()
    {
        $keyStore = $this->_setupTestKeyStore();
        $keyStore->write('test', 'hello world :-)');
        $keyStore->write('test', 'Revision2');
        $keyStore->write('test', 'Revision3');
        $result = $keyStore->read('test', 2);

        $this->assertEquals('Revision2', $result);
    }

    public function testRevisionOne()
    {
        $keyStore = $this->_setupTestKeyStore();
        $keyStore->write('test', 'works');
        $result = $keyStore->read('test');

        $this->assertEquals('works', $result);
    }

    public function testRevisionDoesNotExist()
    {
        $this->setExpectedException('KeyStore_Exception');
        $keyStore = $this->_setupTestKeyStore();
        $keyStore->write('test', 'works');
        $result = $keyStore->read('test', 5);
    }

    public function testDelete()
    {
        $keyStore = $this->_setupTestKeyStore();
        $keyStore->write('test', 'delete me!');
        $keyStore->delete('test');

        $result = $keyStore->exists('test');

        $this->assertFalse($result);
    }

    public function testDeleteWithoutHistory()
    {
        $config = $this->_config;
        $config['keepHistory'] = false;

        $keyStore = $this->_setupTestKeyStore();
        $keyStore->write('test', 'delete me!');
        $keyStore->write('test', 'delete me!');
        $keyStore->write('test', 'delete me!');
        $keyStore->write('test', 'delete me!');
        $keyStore->delete('test');

        $result = $keyStore->exists('test');

        $this->assertFalse($result);
    }


    public function testConnectSuccess()
    {
        $keyStore = $this->_setupTestKeyStore();
        $result = $keyStore->connect('root', 'root');

        $this->assertTrue($result);
    }

    public function testConnectFail()
    {
        $keyStore = $this->_setupTestKeyStore();
        $result = $keyStore->connect('root', 'fail');

        $this->assertFalse($result);
    }

    public function testAddDeleteUser()
    {
        $keyStore = $this->_setupTestKeyStore();

        $user = $keyStore->getAuth()->getUser();
        $user->setPassword('test');
        $user->setUserName('test');
        $user->setPermission(array('global' => KeyStore_Auth::USER_PERMISSION_GLOBAL_ROOT));
        $keyStore->getAuth()->addUser($user);

        $result = $keyStore->getAuth()->userExists('test');

        $this->assertTrue($result);

        //delete
        $keyStore->getAuth()->deleteUser('test');
        $result = $keyStore->getAuth()->userExists('test');
        $this->assertFalse($result);
    }

    public function testGetStorePath()
    {
        $keyStore = $this->_setupTestKeyStore();
        $keyStore->write('test', 'test');
        $path = $keyStore->getItemStorePath('test');

        $result = file_exists($path);

        $this->assertTrue($result);
    }

    public function testSchemaPermission()
    {
        $keyStore = $this->_setupTestKeyStore();

        $user = $keyStore->getAuth()->getUser();
        $user->setUserName('testSchemaPermission');
        $user->setPermission(array(
            'schema' => array(
                'test'=> KeyStore_Auth::USER_PERMISSION_SCHEMA_GRANT
            )
        ));

        $permission = $user->getPermission();

        $this->assertEquals(
            KeyStore_Auth::USER_PERMISSION_SCHEMA_GRANT,
            $permission['schema']['test']
        );
    }


    public function testUserPropertyException()
    {
        $this->setExpectedException('KeyStore_Auth_User_Exception');

        $keyStore = $this->_setupTestKeyStore();
        $user = $keyStore->getAuth()->getUser();
        $user->setMissingProperty('void');
    }

    public function testPermissionHandling()
    {
        $keyStore = new KeyStore($this->_config);
        $user = $keyStore->getAuth()->getUser();

        //global root
        $permission = array(
            'global' => KeyStore_Auth::USER_PERMISSION_GLOBAL_ROOT,
            'schema' => array(
                'test'  => KeyStore_Auth::USER_PERMISSION_NO_PERMISSION
            )
        );

        $user->setPermission($permission);
        $permissionOk = true;
        $permissionOk = (bool) ($permissionOk & $keyStore->getAuth()->checkPermission(KeyStore_Auth::USER_PERMISSION_READ, $user));
        $permissionOk = (bool) ($permissionOk & $keyStore->getAuth()->checkPermission(KeyStore_Auth::USER_PERMISSION_WRITE, $user));
        $permissionOk = (bool) ($permissionOk & $keyStore->getAuth()->checkPermission(KeyStore_Auth::USER_PERMISSION_DELETE, $user));
        $permissionOk = (bool) ($permissionOk & $keyStore->getAuth()->checkPermission(KeyStore_Auth::USER_PERMISSION_CREATE, $user));
        $permissionOk = (bool) ($permissionOk & $keyStore->getAuth()->checkPermission(KeyStore_Auth::USER_PERMISSION_DROP, $user));
        $permissionOk = (bool) ($permissionOk & $keyStore->getAuth()->checkPermission(KeyStore_Auth::USER_PERMISSION_GRANT, $user));
        $this->assertTrue($permissionOk);

        $permissionOk = true;
        $permissionOk = (bool) ($permissionOk & $keyStore->getAuth()->checkPermission(KeyStore_Auth::USER_PERMISSION_READ, $user, 'test'));
        $permissionOk = (bool) ($permissionOk & $keyStore->getAuth()->checkPermission(KeyStore_Auth::USER_PERMISSION_WRITE, $user, 'test'));
        $permissionOk = (bool) ($permissionOk & $keyStore->getAuth()->checkPermission(KeyStore_Auth::USER_PERMISSION_DELETE, $user, 'test'));
        $permissionOk = (bool) ($permissionOk & $keyStore->getAuth()->checkPermission(KeyStore_Auth::USER_PERMISSION_CREATE, $user, 'test'));
        $permissionOk = (bool) ($permissionOk & $keyStore->getAuth()->checkPermission(KeyStore_Auth::USER_PERMISSION_DROP, $user, 'test'));
        $permissionOk = (bool) ($permissionOk & $keyStore->getAuth()->checkPermission(KeyStore_Auth::USER_PERMISSION_GRANT, $user, 'test'));
        $this->assertTrue($permissionOk);

        //schema root
        $permission = array(
            'global' => KeyStore_Auth::USER_PERMISSION_NO_PERMISSION,
            'schema' => array(
                'test'  => KeyStore_Auth::USER_PERMISSION_SCHEMA_ROOT
            )
        );
        $user->setPermission($permission);

        $permissionOk = true;
        $permissionOk = (bool) ($permissionOk & $keyStore->getAuth()->checkPermission(KeyStore_Auth::USER_PERMISSION_READ, $user, 'test'));
        $permissionOk = (bool) ($permissionOk & $keyStore->getAuth()->checkPermission(KeyStore_Auth::USER_PERMISSION_WRITE, $user, 'test'));
        $permissionOk = (bool) ($permissionOk & $keyStore->getAuth()->checkPermission(KeyStore_Auth::USER_PERMISSION_DELETE, $user, 'test'));
        $permissionOk = (bool) ($permissionOk & $keyStore->getAuth()->checkPermission(KeyStore_Auth::USER_PERMISSION_CREATE, $user, 'test'));
        $permissionOk = (bool) ($permissionOk & $keyStore->getAuth()->checkPermission(KeyStore_Auth::USER_PERMISSION_DROP, $user, 'test'));
        $permissionOk = (bool) ($permissionOk & $keyStore->getAuth()->checkPermission(KeyStore_Auth::USER_PERMISSION_GRANT, $user, 'test'));
        $this->assertTrue($permissionOk);
    }

    /**
     * set up the key store instance for testing
     * @return \KeyStore
     */
    private function _setupTestKeyStore($config = null, $login = true)
    {
        if (is_null($config) == true) {
            $config = $this->_config;
        }

        $keyStore = new KeyStore($config);

        if ($login == true) {
            $keyStore->connect('root', 'root');
        }

        return $keyStore;
    }
}